import torch
import torch.nn as nn
from scipy.io import loadmat, savemat

fs = '/'

def load_datasets(folder_data='matlab'):
    # Loads the estimation, validation and test data sets
    estdatamat =   loadmat(folder_data + fs + 'data_train')
    valdatamat =   loadmat(folder_data + fs + 'data_valid')
    testdatamat =  loadmat(folder_data + fs + 'data_test')
    testudatamat = loadmat(folder_data + fs + 'data_testu')

    u_est = torch.tensor(data=estdatamat['U'], dtype=torch.float)
    y_est = torch.tensor(data=estdatamat['Y'], dtype=torch.float)
    p_est = torch.tensor(data=estdatamat['P'], dtype=torch.float)

    u_val = torch.tensor(data=valdatamat['U'], dtype=torch.float)
    y_val = torch.tensor(data=valdatamat['Y'], dtype=torch.float)
    p_val = torch.tensor(data=valdatamat['P'], dtype=torch.float)

    u_tst = torch.tensor(data=testdatamat['U'], dtype=torch.float)
    y_tst = torch.tensor(data=testdatamat['Y'], dtype=torch.float)
    p_tst = torch.tensor(data=testdatamat['P'], dtype=torch.float)

    u_tstu = torch.tensor(data=testudatamat['U'], dtype=torch.float)
    y_tstu = torch.tensor(data=testudatamat['Y'], dtype=torch.float)
    p_tstu = torch.tensor(data=testudatamat['P'], dtype=torch.float)

    # construction of data sets and return
    est_set = (u_est, p_est, y_est)
    val_set = (u_val, p_val, y_val)
    tst_set = (u_tst, p_tst, y_tst)
    tstu_set = (u_tstu, p_tstu, y_tstu)
    return est_set, val_set, tst_set, tstu_set


class LPV_LFR_ANN(nn.Module):
    def __init__(self, n_in=6, n_state = 8, n_out=5, n_sched=3, n_neurons=64, activation=nn.Tanh, initial_gain=0.1):
        super(LPV_LFR_ANN, self).__init__()
        assert n_state > 0
        self.n_in = n_in
        self.n_state = n_state
        self.nx = self.n_state
        self.n_sched = n_sched
        self.n_out = n_out
        self.n_neurons = n_neurons
        self.activation = activation()
        # LFR-ANN matrices for the LTI part. The feedthrough between w and z is assumed to be zero
        self.A = nn.Parameter(data=initial_gain * torch.randn((self.n_state, self.n_state, self.n_sched+1)))
        self.Bu = nn.Parameter(data=initial_gain * torch.randn((self.n_state, self.n_in, self.n_sched+1)))
        self.Bw = nn.Parameter(data=initial_gain * torch.randn((self.n_state, self.n_neurons, self.n_sched+1)))
        self.Cy = nn.Parameter(data=initial_gain * torch.randn((self.n_out, self.n_state, self.n_sched+1)))
        self.Cz = nn.Parameter(data=initial_gain * torch.randn((self.n_neurons, self.n_state, self.n_sched+1)))
        self.Dyu = nn.Parameter(data=initial_gain * torch.randn((self.n_out, self.n_in, self.n_sched+1)))
        self.Dyw = nn.Parameter(data=initial_gain * torch.randn((self.n_out, self.n_neurons, self.n_sched+1)))
        self.Dzu = nn.Parameter(data=initial_gain * torch.randn((self.n_neurons, self.n_in, self.n_sched+1)))
        # biasses
        self.bx = nn.Parameter(data=initial_gain * torch.randn(self.n_state, self.n_sched+1))
        self.bz = nn.Parameter(data=initial_gain * torch.randn(self.n_neurons, self.n_sched+1))
        self.by = nn.Parameter(data=initial_gain * torch.randn(self.n_out, self.n_sched+1))

def cayley(X, Y, Z=None):
    M = X.transpose(1,2) @ X + Y.transpose(1,2) - Y 
    if Z is not None:
        M += Z.transpose(1,2) @ Z 
    I = torch.eye(X.shape[1], dtype=X.dtype, device=X.device)[None, :, :]
    Minv = torch.inverse(I + M)
    if Z is None:
        return (I-M) @ Minv
    else:
        return torch.cat((I-M, -2*Z),axis=1) @ Minv


# default schedule mapping 
class fsched(nn.Module):
    def __init__(self, fin, fout_shp, nh=50, polar=False):
        super().__init__()
        self.np = fin 
        self.shp = fout_shp if isinstance(fout_shp, tuple) else (fout_shp,)
        self.polar = polar

        self.mtx = nn.Sequential(
            nn.Linear(fin, nh),
            nn.ReLU(),
            nn.Linear(nh, nh),
            nn.ReLU(),
            nn.Linear(nh, fout_shp[0] * fout_shp[1] if isinstance(fout_shp, tuple) else fout_shp)
        )
        if polar:
            self.rho = nn.Sequential(
            nn.Linear(fin, nh),
            nn.ReLU(),
            nn.Linear(nh, nh),
            nn.ReLU(),
            nn.Linear(nh, 1)
        )

    def forward(self, p):
        x = self.mtx(p)
        if self.polar:
            q = self.rho(p).view(-1) / torch.norm(x,dim=(1))
            x = q.diag() @ x
        if len(self.shp) == 1: out = x
        else: out = x.view(p.shape[0], self.shp[0], self.shp[1])
        return out


# "CRLPV": Cayley transformation based Robust LPV model 
class CRLPV(nn.Module): 
    def __init__(self, nu, np, nx, ny, sched=fsched, bias=True, gamma=1.0):
        super().__init__()
        self.np = np
        self.nu = nu 
        self.nx = nx 
        self.ny = ny 
        n0 = min(nu, ny)
        n1 = max(nu,ny) - n0
        if ny >= nu:
            self.w_transpose = False 
        else:
            self.w_transpose = True 
        self.xs = sched(np, (nx+n0, nx+n0), polar=True)
        self.ys = sched(np, (nx+n0, nx+n0), polar=True)
        if n1 > 0:
            self.zs = sched(np, (n1, nx+n0), polar=True)
        else: 
            self.zs = None 
        self.bias = bias
        if bias:
            self.bys = sched(np, (ny, 1))
            self.bhs = sched(np, (nx, 1))
        self.gamma = gamma
        self.L = nn.Parameter(torch.zeros(nx))

    def forward(self, xt, ut, pt):
        nx, nu, ny = self.nx, self.nu, self.ny 
        g = self.gamma
        X = self.xs(pt)
        Y = self.ys(pt)
        if self.zs is not None:
            Z = self.zs(pt)
        else:
            Z = None 
        W = cayley(X,Y,Z=Z)
        if self.w_transpose is True:
            W = W.transpose(1,2)
        W = torch.cat((torch.exp(-self.L), g*torch.ones(ny)),axis=0).diag().type(ut.dtype) @ W @ torch.cat((torch.exp(self.L),torch.ones(nu)),axis=0).diag().type(ut.dtype)
        A = W[:,:nx,:nx]
        B = W[:,:nx,nx:]
        C = W[:,nx:,:nx]
        D = W[:,nx:,nx:]
        yt = C @ xt[:,:,None] + D @ ut[:,:,None]
        xn = A @ xt[:,:,None] + B @ ut[:,:,None]
        if self.bias:
            yt += self.bys(pt)
            xn += self.bhs(pt)
             
        return yt.view(pt.shape[0],-1), xn.view(pt.shape[0],-1)


# Functions required for training
def train(dataset, model, loss_fn, optimizer, T=None, Tinit=100):
    batchsize = dataset[0].shape[0]
    if T is not None: pass
    else: T = dataset[0].shape[2]
    y = []
    xk = torch.rand(batchsize,model.nx)
    # forward simulate
    for k in range(T):
        yk, xk = model(xk, dataset[0][:,:,k],dataset[1][:,:,k])
        if torch.any(torch.abs(yk)>1e6):
            T = k
            print(f'NaN obtained in timestep {k} training... doing it with what we have now...')
            break
        y.append(yk)
    ypred = torch.stack(y,dim=2) # compose all the predictions
    loss = loss_fn(ypred[:,:,Tinit:], dataset[2][:,:,Tinit:T]) # Todo: What shall we do with the initial condition in the loss?

    # Backpropagation
    optimizer.zero_grad()
    loss.backward()
    optimizer.step()
    print(f"Train loss: {loss.item():>8f} \n")

def test(dataset, model, loss_fn, T=None, Tinit=100):
    batchsize = dataset[0].shape[0]
    if T is not None: pass # Todo: Splitting up in sections not implemented yet... do we want that?
    else: T = dataset[0].shape[2]
    y = []
    xk = torch.rand(batchsize, model.nx)
    with torch.no_grad():
        for k in range(T):
            yk, xk = model(xk, dataset[0][:, :, k], dataset[1][:, :, k])
            y.append(yk)
        test_loss = loss_fn(torch.stack(y,dim=2)[:,:,Tinit:], dataset[2][:,:,Tinit:T]).item()
    print(f"Test loss: {test_loss:>8f} \n")