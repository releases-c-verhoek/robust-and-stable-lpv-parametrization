import torch
from torch import nn
import math 
import matplotlib.pyplot as plt

from library import *


if __name__ == "__main__":
    # Some parameters
    state_dimension_model = 3 # equivalent to original model
    Tinitskip = 10
    epochs = 20
    CRLPVtrue_LPVRNNfalse = True

    # load data from simple system
    estimation, validation, testset, testu = load_datasets()

    # Get system parameters:
    batchsize, nu, Nest = estimation[0].shape
    _, np, Nval = validation[1].shape
    _, ny, Ntst = testset[2].shape

    # Create model
    if CRLPVtrue_LPVRNNfalse:
        model = CRLPV(nu,np,state_dimension_model,ny, gamma=5)
    else:
        model = LPV_LFR_ANN(n_in=nu, n_out=ny, n_state=state_dimension_model, n_sched=np, n_neurons=100, activation=nn.ReLU)
    
    # define loss function
    loss_function = nn.MSELoss()
    optimizer = torch.optim.Adam(model.parameters(), lr=1e-2)
    for epoch in range(epochs):
        print(f"Epoch {epoch + 1}\n-------------------------------")
        train(estimation, model, loss_function, optimizer,Tinit=Tinitskip)
        test(validation, model, loss_function,Tinit=Tinitskip)
    print("Done!")


    ypred = torch.zeros(testset[2].shape)
    xk = torch.rand((ypred.shape[0],model.nx))

    # forward simulate
    for k in range(testset[2].shape[2]):
        yk, xk = model(xk, testset[0][:, :, k], testset[1][:, :, k])
        ypred[:, :, k] = yk
    plt.figure(figsize=(5, 2.7), layout='constrained')
    plt.plot(ypred[0,:,:].data.reshape((-1,)))
    plt.plot(testset[2][0, :, :].data.reshape((-1,)))
    plt.show()
