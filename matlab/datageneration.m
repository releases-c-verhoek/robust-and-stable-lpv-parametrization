clearvars; close all; clc; rng(1);

% ADD LPVcore TO PATH (available via https://lpvcore.net)
assert(exist('pmatrix','class')==8,'Please add LPVCORE to path...');

%% parameters
np = 3;
nx = 3;
nu = 1;
ny = 1;

noisevariance = 0.08;
batchsize = 64;
Tsim = 200;
Ntrain = 50;
Nvalid = 20;
Ntest  = 30;


%% Define system
% scheduling
p1 = preal('p1', 'dt', 'Range', [-1, 1]);
p2 = preal('p2', 'dt', 'Range', [0, 4]);
p3 = preal('p3', 'dt', 'Range', [-2, 2]);

% eigenvalues
l1 = 1/6 + 0.5*p1 + 1/6*p3;
l2 = -0.5 + p2/3;
l3 = 0.3*(p1+0.5*p3+0.5*p2-1);

% similarity transform
T = randn(nx);

% matrices
A = T*0.9*diag([l1,l2,l3])/T;
B = T*([1;2;1]+[0;0;1]*p2 + [0;1;0]*p1);
C = 0.1*[1, 1, 1]/T;
D = 0.1*(3+ 0.1*p1 + 0.4*p3);

% state-space realization
sys = lpvss(A,B,C,D,-1);

%% Create data sets
% Create training sets
create_data_set('train',sys,Ntrain*batchsize, Tsim, noisevariance)

% Create validation sets
create_data_set('valid',sys,Nvalid*batchsize, Tsim, noisevariance)

% Create test set
create_data_set('test',sys,Ntest, Tsim, noisevariance)
create_data_set('testu',sys,1, Ntest*Tsim, noisevariance)


%% Local functions
function create_data_set(type,sys,batchsize, N, noisevariance)
    if nargin<5; noisevariance = 0; end
    if noisevariance == 0; sffx = '_nonoise.mat';
    else; sffx = '.mat'; end
    
    % input params for multisine
    Nperiod = 5;
    period = N/Nperiod;
    if strcmp(type, 'testu')
        range = [-20, 20];
        K = 1;
    else
        range = [-1, 1];
        K = 0.3;
    end
    % create inputs
    U = zeros(batchsize, sys.Nu, N);
    for i = 1:batchsize
        for k = 1:sys.Nu
            [umsi,~] = idinput([period 1 Nperiod],'sine',[0,1],range);
            U(i,k,:) = umsi'+ 0.05*randn(1, N);
        end
    end
    P = zeros(batchsize, sys.Np, N);
    Y = zeros(batchsize, sys.Ny, N);
    % create batch trajectories
    for i = 1:batchsize
        while true % ensure that the data is within reasonable bounds
            P(i,:,:) = construct_p_rand(N,K);
            [ybatch,~,~] = lsim(sys,reshape(P(i,:,:),N,sys.Np), reshape(U(i,:,:),N,sys.Nu), 'x0', randn(sys.Nx,1));
            if all(abs(ybatch)<15,'all') || strcmp(type, 'testu')
                Y(i,:,:) = reshape(ybatch+noisevariance*randn(N,sys.Ny),sys.Ny, N);
                break
            end
        end
    end
    % save data
    save(['data_',type, sffx], 'U', 'Y', 'P','-mat');
    % clear variables
    clear U Y P
end

function p = construct_p_rand(N, K)
    if nargin < 2; K = 0.3; end
    p = K*[2*rand(1,N)-1;4*rand(1,N); 4*rand(1,N)-2];
end