clearvars; close all; clc;

% ADD LPVcore TO PATH (available via https://lpvcore.net)
assert(exist('pmatrix','class')==8,'Please add LPVCORE to path...');

% load scheduling parameters
load("lpvsystem.mat","p1", "p2", "p3", "nx")

% load data
est =  load('data_train.mat');
tst =  load('data_test.mat');
tstu = load('data_testu.mat');

%% identification
% make model structure (affine parametrization on p)
model = lpvidss(rand(nx)+rand(nx)*p1+rand(nx)*p2+rand(nx)*p3, ... A matrix
        rand(nx,1)+rand(nx,1)*p1+rand(nx,1)*p2+rand(nx,1)*p3, ... B matrix
        rand(1,nx)+rand(1,nx)*p1+rand(1,nx)*p2+rand(1,nx)*p3, ... C matrix
        rand + rand*p1 + rand*p2 + rand*p3, ... D matrix
        'innovation',zeros(nx,1), [], 1,1); 

idopts = lpvssestOptions;

disp('start identification')
for i = 1:5 % use first 5 training data-sets for LPV identification
    tic;
    uest = reshape(est.U(i,:,:), [], size(est.U,2));
    pest = reshape(est.P(i,:,:), [], size(est.P,2));
    yest = reshape(est.Y(i,:,:), [], size(est.Y,2));
    idest = lpviddata(yest,pest,uest,1);
    model = lpvssest(idest, model, idopts);
    model.Ts = 1; % necessity
    % now use last estimate as template for next estimation step
    idopts.Initialization = 'template';
    tm = toc;
    fprintf('Iteration %i (took %.1f seconds)\n', i, tm);
end

%% validation on test set
% 200 steps
utst = reshape(tst.U(1,:,:), [], size(tst.U,2));
ptst = reshape(tst.P(1,:,:), [], size(tst.P,2));
ytst = reshape(tst.Y(1,:,:), [], size(tst.Y,2));
idtst1 = lpviddata(ytst,ptst,utst,1);

% more steps
utstu = reshape(tstu.U(1,:,:), [], size(tstu.U,2));
ptstu = reshape(tstu.P(1,:,:), [], size(tstu.P,2));
ytstu = reshape(tstu.Y(1,:,:), [], size(tstu.Y,2));
idtst2 = lpviddata(ytstu,ptstu,utstu,1);

figure;
compare(idtst1,model,Inf)
figure;
compare(idtst2,model,Inf)


